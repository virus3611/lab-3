﻿using System;

namespace Lab_3._2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter number of iterations n = ");
            int n = int.Parse(Console.ReadLine());
            double summa = 0;
            for (int k = 1; k <= n; k++)
            {
                if (k != 5)
                {
                    summa = 1 / Math.Pow(k, 2);
                }
            }
            Console.WriteLine("summa = {0}" , summa);
        }
    }
}