﻿using System;

namespace Lab_3._3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter n: ");
            int n = int.Parse(Console.ReadLine());
            double result = 0;
            for (int external = 1; external <= n; external++)
            {
                int denominator = 0;
                for (int inner = 1; inner <= external; inner++)
                    denominator += inner;
                result += 1.0 / denominator;
            }

            Console.WriteLine(result);
        }
        
    }
}