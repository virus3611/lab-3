﻿using System;

namespace Lab_3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            double resultF;
            Console.Write("Enter integer number x = ");
            int x = int.Parse(Console.ReadLine());
            Console.Write("Enter double number: ");
            Console.Write("a = ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("b = ");
            double b = double.Parse(Console.ReadLine());
            Console.Write("c = ");
            double c = double.Parse(Console.ReadLine());

            if (x + 5 < 0 & c == 0) {
                resultF = 1 / (a * x) - b;
                Output_Resulting(resultF);
            }
            else if (x + 5 > 0 & c != 0) {
                resultF = (x - a) / c;
                Output_Resulting(resultF);
            }
            else {
                resultF = 10 * x / (c - 4);
                Output_Resulting(resultF);

            }
        }
        

        public static void Output_Resulting(double resultF) {
            Console.WriteLine("Result F = {0}", resultF);

        }
    }
}
